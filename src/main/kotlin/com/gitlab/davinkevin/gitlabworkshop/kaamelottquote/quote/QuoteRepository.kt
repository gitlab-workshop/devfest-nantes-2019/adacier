package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.quote

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface QuoteRepository : JpaRepository<Quote, Long> {
    fun findByBody(body: String): Quote?
}
